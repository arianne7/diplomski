﻿using LostAndFound.Model;
using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LostAndFound.Controller
{
    public class UserController
    {
        IMobileServiceTable<User> usersTable;
        List<User> users = new List<User>();

        public List<User> Users { get => users; set => users = value; }


        public UserController()
        {
            usersTable = App.MobileService.GetTable<User>();
            GetUsersAsync();

        }
        public async void AddNewUserAsync(User user)
        {
            // provjeriti da li radi
            User newUser = user;
            users.Add(user);

            try
            {
                CurrentPlatform.Init();
                await App.MobileService.GetTable<User>().InsertAsync(user);
                // await MobileService.GetTable<User>().InsertAsync(user);
            }
            catch (Exception e)
            {

            }

        }

        public async void GetUsersAsync()
        {
            Users = await usersTable.Where(u => u.Role != null).ToListAsync();
        }

        public User GetUserWithId(string userId)
        {
            GetUsersAsync();
            return Users.Find(u => u.Id.Equals(userId));
        }

        public string UserLoginExists(string username, string password)
        {
            // password treba biti hashiran
            List<User> results = users.FindAll(u => (u.Username.Equals(username) || u.Email.Equals(username)) && u.Password.Equals(password));
           // int results = users.FindAll(u => (u.Username.Equals(username) || u.Email.Equals(username)) && u.Password.Equals(password)).Count;
            if (results.Count > 1)
                throw new Exception("Invalid user");
            else if(results.Count == 1)                
                return users[0].Id;

            return "";
        }

        
        public void DeleteUser(int userId)
        {
            // ovo provjeriti da li ispravno radi
            // vjerovatno ce izbacivati enumeration error
            users.Remove(users.Find(u => u.Id.Equals(userId)));
        }
        public void UpdateUser(int userId, User newData)
        {
            User user = users.Find(u => u.Id.Equals(userId));
            // skontat logiku editovanja usera
        }
    }
}

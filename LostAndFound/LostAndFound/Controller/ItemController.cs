﻿using LostAndFound.Model;
using LostAndFound.Services;
using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace LostAndFound.Controller
{
    public class ItemController
    {
        public static int counter = 0;
        List<Item> items = new List<Item>();
        IMobileServiceTable<Item> itemsTable;


        public ItemController()
        {
            
            itemsTable = App.MobileService.GetTable<Item>();
            GetItemsAsync();
        }
        
        public async void AddLostItemAsync(LostItem lostItem, byte[] imgarray)
        {
            items.Add(lostItem);
            try
            {
                Item newItem = new Item
                {
                    Name = lostItem.Name,
                    Description = lostItem.Description,
                    Note = lostItem.Note,
                    Category = lostItem.Category,
                    ImageUrl = lostItem.ImageUrl,
                    Location = null,
                    ItemType = "lost",
                    ImagePath = lostItem.ImagePath,
                    ContactEmail = lostItem.ContactEmail
                };
                CurrentPlatform.Init();

                string[] names = newItem.ImagePath.Split('/');
                string name = names[names.Length - 1];
                var blockBlob = await BlobStorageService.SaveBlockBlob("photos", imgarray, name);
                counter++;
                newItem.ImageUrl = ImageSource.FromUri(blockBlob.Uri).ToString().Substring(4);
                
                await App.MobileService.GetTable<Item>().InsertAsync(newItem);
            }
            catch (Exception e)
            {

            }
        }

        public async void AddFoundItemAsync(FoundItem foundItem, byte[] imgarray)
        {
            items.Add(foundItem);
            try
            {
                Item newItem = new Item
                {
                    Name = foundItem.Name,
                    Description = foundItem.Description,
                    Note = foundItem.Note,
                    Category = foundItem.Category,
                    ImageUrl = foundItem.ImageUrl,
                    Location = foundItem.Location,
                    DateFound = foundItem.DateFound,
                    ItemType = "found",
                    ImagePath = foundItem.ImagePath,
                    ContactEmail = foundItem.ContactEmail
                    
                };
                CurrentPlatform.Init();
               
                
                string[] names = newItem.ImagePath.Split('/');
                string name = names[names.Length - 1];
                var blockBlob = await BlobStorageService.SaveBlockBlob("photos", imgarray, name);
                counter++;
                newItem.ImageUrl = ImageSource.FromUri(blockBlob.Uri).ToString().Substring(4);

                await App.MobileService.GetTable<Item>().InsertAsync(newItem);
            }
            catch (Exception e)
            {

            }
        }

        public async void GetItemsAsync()
        {
            Items = await itemsTable.Where(i => i.Id != null).ToListAsync();
        }

        public List<Item> getLostItems()
        {
            GetItemsAsync();
            List<Item> lostItems = Items.FindAll(i => i.ItemType.Contains("lost"));            
            return lostItems;
        }

        public List<Item> getFoundItems()
        {
            GetItemsAsync();
            List<Item> foundItems = Items.FindAll(i => i.ItemType.Contains("found"));
            return foundItems;
        }

        public List<Item> getLostItemsByCategory(string category)
        {
            GetItemsAsync();
            List<Item> lostItems = new List<Item>();
            foreach(Item i in Items)
            {
                if (String.Compare(i.Category, category) == 0 && String.Compare(i.ItemType, "lost") == 0)
                    lostItems.Add(i);
            }
                //Items.FindAll(i => i.ItemType.Contains("lost") && i.Category.Contains("Bags"));
            return lostItems;
        }

        public List<Item> getFoundItemsByCategory(string category)
        {
            GetItemsAsync();
            List<Item> foundItems = new List<Item>();
            foreach (Item i in Items)
            {
                if (String.Compare(i.Category, category) == 0 && String.Compare(i.ItemType, "found") == 0)
                    foundItems.Add(i);
            }
            return foundItems;
        }
        public List<Item> searchItemsByName(string name)
        {
            return Items.FindAll(item => item.Name.Contains(name));
        }
        public List<Item> searchItemsByDescription(string description)
        {
            return Items.FindAll(item => item.Description.Contains(description));
        }
        public List<Item> Items { get => items; set => items = value; }
    }
}

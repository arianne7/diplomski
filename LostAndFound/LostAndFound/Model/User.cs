﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LostAndFound.Model
{
    public class User
    {
        public string Id { get; set; }

        [JsonProperty(PropertyName = "username")]
        public string Username { get; set; }

        [JsonProperty(PropertyName = "password")]
        public string Password { get; set; }

        [JsonProperty(PropertyName = "firstName")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "lastName")]
        public string LastName { get; set; }

        [JsonProperty(PropertyName = "Email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "address")]
        public string Address { get; set; }

        [JsonProperty(PropertyName = "phoneNumber")]
        public string PhoneNumber { get; set; }

        [JsonProperty(PropertyName = "role")]
        public string Role { get; set; }

        public User(string username, string password, string firstname, string lastname, string email, string address, string phone, string role)
        {

            Username = username;
            Password = password;
            FirstName = firstname;
            LastName = lastname;
            Email = email;
            Address = address;
            PhoneNumber = phone;
            Role = role;
        }

        /*
        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }
        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public string Email { get => email; set => email = value; }
        public string Address { get => address; set => address = value; }
        public string PhoneNumber { get => phoneNumber; set => phoneNumber = value; }
        public string Role { get => role; set => role = value; }
        public int Id { get => id; set => id = value; }
        */
    }
}

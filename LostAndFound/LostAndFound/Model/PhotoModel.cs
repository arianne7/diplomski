﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LostAndFound.Model
{
    public class PhotoModel
    {
        public System.Uri Uri { get; set; }
        public string Title { get; set; }
    }
}

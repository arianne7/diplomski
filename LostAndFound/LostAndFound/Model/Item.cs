﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Net.Mime;
using Newtonsoft.Json;

namespace LostAndFound.Model
{
    public class Item
    {

        public string Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }

        [JsonProperty(PropertyName = "category")]
        public string Category { get; set; }

        [JsonProperty(PropertyName = "imageurl")]
        public string ImageUrl { get; set; }

        [JsonProperty(PropertyName = "location")]
        public string Location { get; set; }

        [JsonProperty(PropertyName = "dateFound")]
        public DateTime DateFound { get; set; }

        [JsonProperty(PropertyName = "itemType")]
        public string ItemType { get; set; }

        [JsonProperty(PropertyName = "path")]
        public string ImagePath { get; set; }

        [JsonProperty(PropertyName = "contactEmail")]
        public string ContactEmail { get; set; }

        public Item()
        {

        }
        public Item(string name,string category, string description, string note)
        {           
            Name = name;
            Category = category; 
            Description = description;
            Note = note;
        }
        public Item(string name, string category, string description, string note, string url)
        {
            Name = name;
            Category = category;
            Description = description;
            Note = note;
            ImageUrl = url;
        }
        
        public List<Item> GetItems()
        {
            List<Item> items = new List<Item>()
            {
                new Item("wallet", "Wallets" , "kozni novcanik", "nema naknade", "wallet.jpg"),
                new Item("usb stick", "Devices", "", "sentimental value", "usb.jpg")
            };
            return items;
        }

    }
}

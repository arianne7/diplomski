﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LostAndFound.Model
{
    public class FoundItem : Item
    {
        string location;
        DateTime dateFound;

        public string Location { get => location; set => location = value; }
        public DateTime DateFound { get => dateFound; set => dateFound = value; }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LostAndFound.Model
{
    public class TodoItem
    {
        
        public string Id { get; set; }

        [JsonProperty(PropertyName = "text")]
        public string Text { get; set; }
    }
}

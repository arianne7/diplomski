﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LostAndFound.Model
{
    public class Category
    {
        private string name;
        private string iconUrl;

        public Category(string name, string imageurl)
        {
            Name = name;
            IconUrl = imageurl;
            
        }

        public string Name { get => name; set => name = value; }
        public string IconUrl { get => iconUrl; set => iconUrl = value; }
    }
}

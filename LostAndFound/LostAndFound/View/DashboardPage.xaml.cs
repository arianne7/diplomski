﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LostAndFound
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DashboardPage : MasterDetailPage
    {
        public static string UserId;
        public DashboardPage(string id)
        {
            InitializeComponent();
            UserId = id;
            MasterPage.ListView.ItemSelected += ListView_ItemSelected;
        }


        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (!(e.SelectedItem is DashboardPageMenuItem item))
                return;

            var page = (Page)Activator.CreateInstance(item.TargetType);
            page.Title = item.Title;

            Detail = new NavigationPage(page);
            //Detail = new AddLostItemPage();
            IsPresented = false;
            MasterPage.ListView.SelectedItem = null;
        }
    }
}
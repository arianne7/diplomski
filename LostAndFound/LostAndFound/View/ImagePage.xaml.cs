﻿using LostAndFound.Model;
using LostAndFound.Services;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LostAndFound.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ImagePage : ContentPage
    {
        readonly Label _title = new Label { HorizontalTextAlignment = TextAlignment.Center };
        readonly Image _image = new Image();
        readonly ActivityIndicator _activityIndicator = new ActivityIndicator();

        public ImagePage()
        {
            InitializeComponent();
            Content = new StackLayout
            {
                Spacing = 15,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                Children = {
                _title,
                _image,
                _activityIndicator
            }
            };

            Title = "Image Page";
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            _activityIndicator.IsRunning = true;

            var blobList = await BlobStorageService.GetBlobs<CloudBlockBlob>("photos");

            var photo = new PhotoModel();
            //= blobList?.FirstOrDefault();
            string uris = "";
            foreach (var blob in blobList)
            {
                uris += blob.Uri + "/n";
                if (String.Compare(blob.Name, "cat.png") == 0)
                {

                    photo.Title = blob.Name;
                    photo.Uri = blob.Uri;
                }
            }
            //var photo = new PhotoModel { Title = firstBlob?.Name, Uri = firstBlob?.Uri };

            _title.Text = uris;
            _image.Source = ImageSource.FromUri(photo?.Uri);

            _activityIndicator.IsRunning = false;
            _activityIndicator.IsVisible = false;
        }
    }
}

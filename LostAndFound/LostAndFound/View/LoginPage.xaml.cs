﻿using LostAndFound.Controller;
using LostAndFound.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LostAndFound
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
        
		public LoginPage ()
		{
			InitializeComponent();
		}


        private void loginButton_Clicked(object sender, EventArgs e)
        {
            string username = usernameEntry.Text;
            string password = passwordEntry.Text;
            string id = App.userController.UserLoginExists(username, password);

            if (id != null)
            {
                App.Current.MainPage = new DashboardPage(id);
                this.Navigation.PushAsync(new DashboardPage(id));
            }
                
        }
    }
}
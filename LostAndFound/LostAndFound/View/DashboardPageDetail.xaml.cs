﻿using LostAndFound.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LostAndFound
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DashboardPageDetail : ContentPage
    {
        public DashboardPageDetail()
        {
            InitializeComponent();
        }
        
        private void buttonCreate_Clicked(object sender, EventArgs e)
        {
            this.Navigation.PushAsync(new AddItemPage());
        }
    }
}
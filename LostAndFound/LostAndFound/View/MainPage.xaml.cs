﻿using LostAndFound.Controller;
using LostAndFound.Model;
using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace LostAndFound
{
	public partial class MainPage : ContentPage
	{
        UserController userController;
        ItemController itemController;


        public MainPage(UserController userController, ItemController itemController)
		{
			InitializeComponent();
            UserController = userController;
            ItemController = itemController;            
		}

        public UserController UserController { get => userController; set => userController = value; }
        public ItemController ItemController { get => itemController; set => itemController = value; }

        private void loginButton_Clicked(object sender, EventArgs e)
        {
            
            //await this.Navigation.PushAsync(new LoginPage());
            App.Current.MainPage = new LoginPage();
        }

        private void signUpButton_Clicked(object sender, EventArgs e)
        {

            App.Current.MainPage = new SignupPage();
        }
    }
}

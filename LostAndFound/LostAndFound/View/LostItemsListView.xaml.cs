﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using LostAndFound.Controller;
using LostAndFound.Model;

namespace LostAndFound.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LostItemsListView : ContentPage {
      
        ItemController itemController;

		public LostItemsListView (string category, string imgres)
		{
			InitializeComponent ();
            itemController = App.itemController;
            List<Item> lostItems = new List<Item>();
            lostItems = itemController.getLostItemsByCategory(category);
            listLostItems.ItemsSource = lostItems;
        }

        private async void listLostItems_ItemSelectedAsync(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
                return;

            var dataItem = e.SelectedItem as LostItem;
            try
            {
                await this.Navigation.PushAsync(new ItemPage(dataItem));
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", ex.Message, "OK");
            }
            ((ListView)sender).SelectedItem = null;
        
        }

        private async void listLostItems_ItemTappedAsync(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null)
                return;

            var dataItem = e.Item as Item;

            try
            {
               
                await this.Navigation.PushAsync(new ItemPage(dataItem));
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", ex.Message, "OK");
            }

            // await DisplayAlert("Item Tapped", dataItem.Name + "selected", "OK");

            //Deselect Item
            ((ListView)sender).SelectedItem = null;

        }
    }
}
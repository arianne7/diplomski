﻿using LostAndFound.Model;
using Plugin.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LostAndFound.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ItemPage : ContentPage
	{
        Item item;
		public ItemPage (Item item)
		{
            
			InitializeComponent ();
            Item = item;
            labelItemName.Text = item.Name;
            itemImagePreview.Source = item.ImageUrl;
            labelItemDescription.Text = item.Description;
            labelItemNote.Text = item.Note;
		}

        public Item Item { get => item; set => item = value; }

        private void buttonContact_Clicked(object sender, EventArgs e)
        {
            // this.Navigation.PushAsync(new ContactPage(item.ContactEmail, DashboardPage.UserId));
            var emailTask = CrossMessaging.Current.EmailMessenger;
            if (emailTask.CanSendEmail)
            {
                // Send simple e-mail to single receiver without attachments, CC, or BCC.
                // emailTask.SendEmail("plugins@xamarin.com", "Xamarin Messaging Plugin", "Hello from your friends at Xamarin!");

                // Send a more complex email with the EmailMessageBuilder fluent interface.
                var email = new EmailMessageBuilder()
                  .To("lostandfoundappservice@gmail.com")
                  .Subject("")
                  .Body("Hello from your friends at Xamarin!")
                  .Build();

                string body = email.Message;
                emailTask.SendEmail(email);

                string emailSender = App.userController.GetUserWithId(DashboardPage.UserId).Email;
                string message = "Hello," + "\n" + body + "\n" + "This email was sent by " + emailSender + "\n" + "You can now contact them.";
                emailTask.SendEmail(item.ContactEmail, "Question about your post", message);

                emailTask.SendEmail(emailSender, "Response", "Thank you for your email. The owner of the post will contact you soon.");


            }
        }
    }
}
using LostAndFound.Controller;
using LostAndFound.View;
using Microsoft.WindowsAzure.MobileServices;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace LostAndFound
{
	public partial class App : Application
	{
        // trebaju biti static ako ce se proslijediti
        public static UserController userController;
        public static ItemController itemController;
        public static MobileServiceClient MobileService = new MobileServiceClient("https://lostandfoundma.azurewebsites.net");


        public App ()
		{
			InitializeComponent();
            userController = new UserController();
            itemController = new ItemController();
            MainPage = new MainPage(userController, itemController);
           // MainPage = new NavigationPage(new ImagePage());
            // MainPage = new LostItemsView();
            // MainPage = new LoginPage();
            //MainPage = new SignupPage();
            //MainPage = new DashboardPage();
            //MainPage = new LostItemsListView();
            //MainPage = new AddItemPage(); // page with picker
            //MainPage = new CategoriesListView();
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

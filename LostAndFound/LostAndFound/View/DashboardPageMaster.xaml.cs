﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using LostAndFound.View;

namespace LostAndFound
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DashboardPageMaster : ContentPage
    {
        public ListView ListView;

        public DashboardPageMaster()
        {
            InitializeComponent();

            BindingContext = new DashboardPageMasterViewModel();
            ListView = MenuItemsListView;
        }

        class DashboardPageMasterViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<DashboardPageMenuItem> MenuItems { get; set; }
            
            public DashboardPageMasterViewModel()
            {
                MenuItems = new ObservableCollection<DashboardPageMenuItem>(new[]
                {
                    new DashboardPageMenuItem { Id = 0, Title = "My profile", TargetType = typeof(MyProfilePage) },
                    new DashboardPageMenuItem { Id = 1, Title = "Add lost item", TargetType = typeof(AddItemPage)},
                    new DashboardPageMenuItem { Id = 2, Title = "Add found item", TargetType = typeof(AddItemPage)},
                    new DashboardPageMenuItem { Id = 3, Title = "Search Lost Items", TargetType = typeof(CategoriesListView) },
                    new DashboardPageMenuItem { Id = 4, Title = "Search Found Items", TargetType = typeof(CategoriesFoundListView)},
                    new DashboardPageMenuItem { Id = 5, Title = "Log Out", TargetType = typeof(MainPage) },
                });
            }
            
            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }
    }
}
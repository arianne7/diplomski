﻿using Plugin.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LostAndFound.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ContactPage : ContentPage
	{
        string emailFrom;
        string emailTo;
		public ContactPage (string r, string userid)
		{
			InitializeComponent ();
            EmailFrom = App.userController.GetUserWithId(DashboardPage.UserId).Email;
            EmailTo = r;
            // sender email
            // App.userController.GetUserWithId(DashboardPage.UserId).Email

            // primaoc - u hidden field-u je
        }

        public string EmailFrom { get => emailFrom; set => emailFrom = value; }
        public string EmailTo { get => emailTo; set => emailTo = value; }

        private void sendMessage_Clicked(object sender, EventArgs e)
        {
            var emailTask = CrossMessaging.Current.EmailMessenger;
            if (emailTask.CanSendEmail)
            {              
                var email = new EmailMessageBuilder()
                  .To("lostandfoundappservice@gmail.com")
                  .Subject("")
                  .Body("Hello from your friends at Xamarin!")
                  .Build();

                emailTask.SendEmail(email);
            }
        }
    }
}
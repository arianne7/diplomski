﻿using Android.Support.V7.App;
using LostAndFound.Model;
using LostAndFound.Services;
using Microsoft.WindowsAzure.Storage.Blob;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LostAndFound.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AddItemPage : ContentPage
	{
        string imagePath;
        Image itemImage;
        object selectedImage;
        private List<string> categories;
        byte[] imgArray;

        public Image ItemImage { get => itemImage; set => itemImage = value; }
        public object SelectedImage { get => selectedImage; set => selectedImage = value; }
        public string ImagePath { get => imagePath; set => imagePath = value; }
        public byte[] ImgArray { get => imgArray; set => imgArray = value; }

        public AddItemPage ()
		{
			InitializeComponent ();
            ItemImage = new Image();
            
            itemPicker.Items.Add("Lost");
            itemPicker.Items.Add("Found");
            // what happens when one of these is selected

            /*
             new Category("Clothes", "clothes.png"),
                new Category("Bags", "bags.jpg"),
                new Category("Wallets", "wallets.png"),
                new Category("Keys", "keys.png"),
                new Category("Devices", "devices.png"),
                new Category("Accessories", "accessories.png")
             */
            categories = new List<string>();
            itemPickerCategory.Items.Add("Clothes");
            categories.Add("Clothes");
            itemPickerCategory.Items.Add("Bags");
            categories.Add("Bags");
            itemPickerCategory.Items.Add("Wallets");
            categories.Add("Wallets");
            itemPickerCategory.Items.Add("Keys");
            categories.Add("Keys");
            itemPickerCategory.Items.Add("Devices");
            categories.Add("Devices");
            itemPickerCategory.Items.Add("Accessories");
            categories.Add("Accessories");

            entryContactEmail.Text = App.userController.GetUserWithId(DashboardPage.UserId).Email;

        }

        private void Button_Clicked(object sender, EventArgs e)
        {

        }

        private void itemPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (itemPicker.SelectedIndex == 0)
                locationEntry.IsVisible = false;
            else
                locationEntry.IsVisible = true;
        }

        private void itemPickerCategory_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void addItemButton_Clicked(object sender, EventArgs e)
        {
            if(itemPicker.SelectedIndex == 0)
            {
                LostItem lostItem = new LostItem
                {
                    Name = entryItemName.Text,
                    Description = editorItemDescription.Text,
                    Note = editorItemNote.Text,
                    Location = null,
                    ItemType = "lost",
                    Category = categories[itemPickerCategory.SelectedIndex],
                    ImagePath = ImagePath,
                    ContactEmail = entryContactEmail.Text
                    
                };
                App.itemController.AddLostItemAsync(lostItem, ImgArray);
                DisplayAlert("Success", "Lost item added.", "Ok");                

            }
            else
            {
                FoundItem foundItem = new FoundItem
                {
                    Name = entryItemName.Text,
                    Description = editorItemDescription.Text,
                    Note = editorItemNote.Text,
                    Location = locationEntry.Text,
                    DateFound = DateTime.Now,
                    ItemType = "found",
                    Category = categories[itemPickerCategory.SelectedIndex],
                    ImagePath = ImagePath,
                    ContactEmail = entryContactEmail.Text
                };
                App.itemController.AddFoundItemAsync(foundItem, ImgArray);
                DisplayAlert("Success", "Found item added.", "Ok");

            }
        }

        private async void uploadImageButton_ClickedAsync(object sender, EventArgs e)
        {
          //  await DisplayAlert("button", "clicked", "ok");
            // Initialize method from the CrossMedia class to establish the communication between the native project and the Media Plugin
            await CrossMedia.Current.Initialize();

            // make sure that the device supports operation
            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                await DisplayAlert("Not suported", "Your device does not suport this functionality", "OK");
                return;
            }

            // change the size if the image is too big
            var mediaOptions = new PickMediaOptions()
            {
                PhotoSize = PhotoSize.Medium
            };

            var selectedImageFile = await CrossMedia.Current.PickPhotoAsync(mediaOptions);
            
            if (selectedImageFile == null)
            {
                await DisplayAlert("Error", "Could not load the image. Please try again.", "OK");
                return;
            }

            ItemImage.Source = ImageSource.FromStream(() => selectedImageFile.GetStream());

            ImagePath = selectedImageFile.Path;
            await DisplayAlert("img path", ImagePath, "OK");
            byte[] imgarray = GetImageStreamAsBytes(selectedImageFile.GetStream());
            ImgArray = imgarray;
       
            //SelectedImage = selectedImageFile;
           // var blobList = await BlobStorageService.SaveBlockBlob("photos", selectedImage.Path, selectedImage);

        }

        private byte[] GetImageStreamAsBytes(Stream input)
        {
            var buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}
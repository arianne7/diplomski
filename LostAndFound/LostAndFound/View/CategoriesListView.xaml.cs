﻿using LostAndFound.Model;
using LostAndFound.Services;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LostAndFound.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CategoriesListView : ContentPage
	{
        List<Category> categories;
		public CategoriesListView ()
		{
			InitializeComponent ();
            Categories = new List<Category>()
            {
                new Category("Clothes", "clothes.png"),
                new Category("Bags", "bags.jpg"),
                new Category("Wallets", "wallets.png"),
                new Category("Keys", "keys.png"),
                new Category("Devices", "devices.png"),
                new Category("Accessories", "accessories.png")
            };
            categoriesList.ItemsSource = Categories;
		}

        public List<Category> Categories { get => categories; set => categories = value; }

        async void categoriesList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null)
                return;

            var dataItem = e.Item as Category;
          //  await DisplayAlert("category", dataItem.Name, "Ok");

            try
            {
                // LostItemsListView lostItemsListView = new LostItemsListView();
                  await this.Navigation.PushAsync(new LostItemsListView(dataItem.Name, ""));
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", ex.Message, "OK");
            }

            // await DisplayAlert("Item Tapped", dataItem.Name + "selected", "OK");

            //Deselect Item
            ((ListView)sender).SelectedItem = null;
        }
    }
}
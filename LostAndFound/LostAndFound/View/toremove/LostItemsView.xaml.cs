﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LostAndFound.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LostItemsView : ContentPage
    {
        public ObservableCollection<string> Items { get; set; }

        public LostItemsView()
        {
            InitializeComponent();

            Items = new ObservableCollection<string>
            {
                "Clothes",
                "Bags",
                "Wallets",
                "Keys",
                "Devices",
                "Accessories"
            };
			
			MyListView.ItemsSource = Items;
        }

        async void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null)
                return;

            //await DisplayAlert("Item Tapped", "An item was tapped.", "OK");
            if (e.Item.ToString().Equals("Clothes"))
                await DisplayAlert("Item Tapped","Clothes tapped", "OK");
            else
                await DisplayAlert("Item Tapped", "Clothes not tapped", "OK");


            //Deselect Item
            ((ListView)sender).SelectedItem = null;
        }
    }
}

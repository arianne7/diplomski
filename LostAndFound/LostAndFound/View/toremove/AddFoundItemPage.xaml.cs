﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LostAndFound.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AddFoundItemPage : ContentPage
	{
		public AddFoundItemPage ()
		{
			InitializeComponent ();
		}

        private void Button_Clicked(object sender, EventArgs e)
        {
            this.Navigation.PushAsync(new DashboardPageDetail());
        }
    }
}
﻿using LostAndFound.Controller;
using LostAndFound.Model;
using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LostAndFound
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SignupPage : ContentPage
	{
      
        public SignupPage ()
		{
			InitializeComponent ();
		}

     
        private void  signupButton_Clicked(object sender, EventArgs e)
        {
            User newUser = new User(usernameEntry.Text,
                                    passwordEntry.Text,
                                    firstNameEntry.Text,
                                    lastNameEntry.Text,
                                    emailEntry.Text,
                                    addressEntry.Text,
                                    phoneNumberEntry.Text,
                                    "user");
            /*
                        User newUser = new User
                        {
                            Username = usernameEntry.Text,
                            Password = passwordEntry.Text,
                            FirstName = firstNameEntry.Text,
                            LastName = lastNameEntry.Text,
                            Email = emailEntry.Text,
                            Address = addressEntry.Text,
                            PhoneNumber = phoneNumberEntry.Text,
                            Role = "user"

                        };*/

            //await userController.AddNewUserAsync(newUser);

            // CurrentPlatform.Init();
            //   await MobileService.GetTable<User>().InsertAsync(newUser);

            App.userController.AddNewUserAsync(newUser);
            App.Current.MainPage = new LoginPage();
        }
    }
}
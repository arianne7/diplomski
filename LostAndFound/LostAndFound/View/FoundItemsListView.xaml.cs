﻿using LostAndFound.Controller;
using LostAndFound.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LostAndFound.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FoundItemsListView : ContentPage
	{
        ItemController itemController;
		public FoundItemsListView (string category)
		{
			InitializeComponent ();
            itemController = App.itemController;
            List<Item> foundItems = new List<Item>();
            foundItems = itemController.getFoundItemsByCategory(category);
            // List<Item> lostItemsCat = new List<Item>();
            //lostItemsCat = lostItems.FindAll(i => i.Category.Equals(category));
            listLostItems.ItemsSource = foundItems;
            //
        }

        private async void listLostItems_ItemTappedAsync(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null)
                return;

            var dataItem = e.Item as Item;

            //  await DisplayAlert("category", dataItem.Name, "Ok");

            try
            {
                // LostItemsListView lostItemsListView = new LostItemsListView();
                //await DisplayAlert("obj", dataItem.Name + "  tapped" , "ok");
                await this.Navigation.PushAsync(new ItemPage(dataItem));
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", ex.Message, "OK");
            }

            // await DisplayAlert("Item Tapped", dataItem.Name + "selected", "OK");

            //Deselect Item
            ((ListView)sender).SelectedItem = null;
        }
    }
}